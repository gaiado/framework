<?php

class Database extends PDO {

    private static $pdo;

    public function __construct($dsn, $username, $passwd, $options=null) {
        parent::__construct($dsn, $username, $passwd, $options);
    }

    public static function getInstance() {
        if (is_null(Database::$pdo)) {
            Database::$pdo = new Database('mysql:host='.DB_HOST.';dbname='.DB_NAME,DB_USER,DB_PASS);
              Database::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }
        return Database::$pdo;
    }

}
