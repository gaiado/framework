<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Bootstrap
 *
 * @author Alejandro
 */
class Bootstrap {

    private static $bootstrap;

    public static function getInstance() {
        if (is_null(Bootstrap::$bootstrap)) {
            Bootstrap::$bootstrap = new Bootstrap();
        }
        return Bootstrap::$bootstrap;
    }

    private function __construct() {
        $type = $_SERVER['REQUEST_METHOD'];
        $url = isset($_GET['url']) ? $_GET['url'] : 'Index';
        $url = explode('/', rtrim($url, '/'));
        $file = 'controllers/' . $url[0] . '.php';
        if (!file_exists($file)) {
            return $this->error();
        }
        require_once 'controllers/' . $url[0] . '.php';
        $controller = new $url[0];
        if (isset($url[1])) {
            $url[1] = $type . $url[1];
            if (!method_exists($controller, $url[1])) {
                return $this->error();
            }
            if (isset($url[2])) {
                return $controller->{$url[1]}($url[2]);
            } else {
                return $controller->{$url[1]}();
            }
        }
        if (method_exists($controller, 'getIndex')) {
            return $controller->getIndex();
        }
    }

    private static function error() {
        require_once 'controllers/Error.php';
        $error = new Error();
        $error->error404();
        return false;
    }

}
