<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of View
 *
 * @author Alejandro
 */
class View {

    protected $template;

    public function __construct($template = null, $params = null) {
        $this->setTemplate($template);
        $this->setParams($params);
    }

    public function setTemplate($template) {
        if (isset($template)) {
            $this->template = $template;
        }
    }

    public function setParams($params) {
        if (isset($params)) {            
            foreach ($params as $key => $value) {
                $this->{$key} = $value;
            }
        }
    }

    public function render() {
        require_once 'views/' . $this->template . '.php';
    }

}
