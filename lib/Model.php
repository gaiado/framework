<?php

Class Model {

    const table = '';
    const key = 'id';
    const columns = '';

    public static function all() {
        $pdo = Database::getInstance();
        $clasehija = get_called_class();
        $stmt = $pdo->prepare('call ' . $clasehija::table . '_' . __FUNCTION__);
        $stmt->execute();
        $objectstmt = $stmt->fetchAll(PDO::FETCH_OBJ);
     
        return $objectstmt;
    }

    public static function find($id) {
        $pdo = Database::getInstance();
        $clasehija = get_called_class();
        $stmt = $pdo->prepare('SELECT * FROM ' . $clasehija::table . ' where ' . $clasehija::key . ' = ' . $id);
        $stmt->execute();
        $objectstmt = $stmt->fetch(PDO::FETCH_OBJ);
        $object = new $clasehija;
        foreach ($objectstmt as $key => $value) {
            $object->{$key} = $value;
        }
        return $object;
    }

    public function save() {
        $vars = get_object_vars($this);
        $indexes = array_keys($vars);
        $clasehija = get_called_class();

        if (isset($vars['id'])) {
            $pdo = Database::getInstance();
            $clasehija = get_called_class();
            $stmt = $pdo->prepare('call ' . $clasehija::table . '_' . __FUNCTION__ . ' (' . $clasehija::columns . ')');
            foreach ($vars as $key => $value) {
                $stmt->bindValue(':' . $key, $value);
            }
            $stmt->execute();
        }
    }

    public function update() {
        $vars = get_object_vars($this);
        $indexes = array_keys($vars);
        $clasehija = get_called_class();
        if (isset($vars['id'])) {
            $pdo = Database::getInstance();
            $clasehija = get_called_class();
            $stmt = $pdo->prepare('call ' . $clasehija::table . '_' . __FUNCTION__ . ' (' . $clasehija::columns . ')');
            foreach ($vars as $key => $value) {
                $stmt->bindValue(':' . $key, $value);
            }
            $stmt->execute();
        }
    }

}
